<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ticket extends LI_Controller {
	
	function __construct() {
		
		parent::__construct();
		$this->_init();
		$this->load->model(
				array(
					'admin/m_ticket',
				));
	}
	
	public function _init(){
		$this->is_sale			= 0; 
		$this->is_active		= 0; 
		$this->is_delete		= 0;
        $this->user_code		= $this->session->userdata('logged_in')['user_code'];
		$this->output->set_content_type('application/json');
		
	}
	 
    public function storage(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');

		$input = $this->input->post(null, true);

		$value = $this->_build_data($input);
		$this->db->trans_begin();
		$result = $this->m_ticket->insert($value);
	
		if ($result){
			$_message = $this->getMessage('ADD');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
		}else{
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
		
		
		
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
    }
    
    public function update(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');

		$input = $this->input->post(null, true);
		$value = $this->_build_data($input);
		$id	   = $input['ticket_no'];

		$this->db->trans_begin();
		$result = $this->m_ticket->update($id, $value);
		
		if ($result){
			$_message = $this->getMessage('ADD');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
		}else{
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
        
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
	}

	public function _build_data($input){
		$_system_date 		= date('Y-m-d H:i:s');

        $value['ticket_name'] 					= $input['ticket_name'];
		$value['ticket_detail'] 				= $input['ticket_detail'];
		$value['ticket_contact_information'] 	= $input['ticket_contact_information'];
	
		$value['ticket_status'] 				= "pending";

        if ( $input['mode'] == 'create' ) {
			$value['system_add_date']	 = $_system_date;
			$value['system_update_date'] = $_system_date;
        } else {
            $value['system_update_date'] = $_system_date;
        }
        return $value;
	}
    
	public function delete(){
		$_message_title 		= 'ข้อความแจ้งเตือน !';
		$_message				= '';
		$_error					= 0;
		$_warning				= '';
		$_error_msg   			= '';
		$_system_date 			= date('Y-m-d H:i:s');

		$_ticket_no	    = $this->setBase64_decode($this->input->post('ticket_no'));
		$item 						= $this->m_ticket->get_ticketById($_ticket_no)->row();
	
		$_item_arr ='';	
	
		if(!empty($_ticket_no)){
			$_item_arr = array('is_delete' => 1,'system_update_date' => $_system_date);
		}

		$this->db->trans_begin();
		$result = $this->db->update('tbl_ticket', $_item_arr, array('ticket_no' => $_ticket_no));
		if ($result){
			$_message = $this->getMessage('DELETE');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
		}else{
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
        
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
	}

	// update สถานะตั๋ว
	public function status(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
		$_status            = 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');
		
		$_ticket_no    		= $this->setBase64_decode($this->input->post('ticket_no'));
		$_ticket_status    	= $this->input->post('ticket_status');
	
		$_item_arr ='';	
		if(!empty($_ticket_no)){
			$this->db->trans_begin();
	
			$_item_arr = array(
				'ticket_status' => $_ticket_status,
				'system_update_date' => $_system_date
			);
			
			$result ='';
			$result = $this->db->update('tbl_ticket', $_item_arr, array('ticket_no' => $_ticket_no));
			
			if ($result){
			$_message = $this->getMessage('EDIT');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
			}else{
				$this->db->trans_rollback();
				$_message = $this->getMessage('ERROR');
				$_warning = 'warning';
				$_status  = 0;
			}		
		}
	
			
		$_data = array(
			'message'  	=> $_message['message_detail']
		   ,'warning'  	=> $_warning
		   ,'status'   	=> $_status
		   ,'title'    	=> $_message_title
		   ,'error_msg'    => $_error_msg
	   );
		
		$this->output->set_output(json_encode($_data));
	}

}
