<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends LI_Controller {
	
	function __construct() {
        parent::__construct();
		
		$this->_set_user_login();
		$this->_init();
		
		$this->load->model(
			array(
				'admin/m_ticket'
			));
	}
	
	public function _init(){
		
		$this->is_active			= 0; 
		$this->is_delete			= 0;
		$this->user_code			= $this->session->userdata('logged_in')['user_code'];
		$this->_data['menuHeader']	= 'admin/ticket';
		$this->_data['menuLineList']= 2;
		$this->_status              = array('pending','accepted','resolved','rejected');
	}
	
	public function _set_user_login(){
		if(empty($this->session->userdata('logged_in'))){
			redirect(base_url().'login');
		}
	}
	
	public function index(){

		// $count 	= $this->m_ticket->countAll();
		// if($count > 0):
		// 	$this->_data['ticket'] = $this->m_ticket->get_ticket()->result();
		// endif;

		// url
		$this->_data['url_create'] = base_url()."admin/".$this->router->fetch_class()."/create";

		$send = array(
			'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'ticket/index'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'ตั๋ว'
			,'data'			=> $this->_data
		);
		
		$this->loadPageView($send);
	}

	public function ticket_data(){
		// POST data
		$input 		= $this->input->post();
		$num_start	= $this->input->post('start')+1;
		$info 		= $this->m_ticket->get_rows($input);
        $infoCount 	= $this->m_ticket->get_count($input);
        $column 	= array();
        
        foreach ($info->result() as $key => $rs) {
			$id = base64_encode($rs->ticket_no);
			
			$btn_action = '<a href="'.base_url().'admin/ticket/edit/'.$id.'"><i class="fas fa-edit"></i></a>
					<a href="javascript:void(0)" data-id="'.$id.'" class="delete-item"><i class="fas fa-trash-alt"></i></a>';
			
			$btn_status = '<select class="form-control-sm btn-secondary status" data-id="'.$id.'">';
			foreach($this->_status as $status){
				$selected = '';
				if($status == $rs->ticket_status):
					$selected = 'selected';
				endif;
				$btn_status.= '<option value="'.$status.'" '.$selected.'>'.$status.'</option>';
			}
			$btn_status.= '</select>';
			
			$column[$key]["number"] 					= $key+$num_start;
			$column[$key]["ticket_name"] 				= $rs->ticket_name;
			$column[$key]["ticket_detail"] 				= $rs->ticket_detail;
			$column[$key]["ticket_contact_information"] = $rs->ticket_contact_information;
			$column[$key]["btn_status"] 				= $btn_status;
			$column[$key]["system_add_date"] 			= $rs->system_add_date;
			$column[$key]["system_update_date"] 		= $rs->system_update_date;
			$column[$key]["btn_action"] 				= $btn_action; 

        }
        $data['aaData'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
	}
	
	public function create(){

		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'ticket/from'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'สร้าง'
			,'mode'			=> 'create'
			,'data'			=> $this->_data
		);
		
		$send['form_action'] = base_url().'api/admin/'.$this->router->fetch_class().'/storage';
		$this->loadPageView($send);
	}
	
	public function edit($id=null){
		
		$id 	 				= $this->setBase64_decode($id);
		$this->_data['ticket'] = $this->m_ticket->get_ticketById($id)->row();

		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'ticket/from'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'แก้ไข'
			,'mode'			=> 'edit'
			,'data'			=> $this->_data
		);

		$send['form_action'] = base_url().'api/admin/'.$this->router->fetch_class().'/update';
		$this->loadPageView($send);
	}

}
