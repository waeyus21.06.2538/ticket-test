<?php
	$_session_in = $this->session->userdata('logged_in');
	$_user_code_head 	= $_session_in['user_code'];
  $_user_name_head 	= $_session_in['user_name'];
  $_user_type_code  = $_session_in['user_type_code'];
	$_user_img 			= base_url().'upload/users/'.$_session_in['user_img'];
?>
<div class="wrapper">
  <!-- <header class="main-header"> -->
  <div class="navbar-bg"></div>
  <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="<?=base_url()?>assets/admin/img/avatar/avatar-1.png" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">admin</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <!-- <div class="dropdown-title">Logged in 5 min ago</div>
              <a href="<?=base_url()?>admin/usersad/profile" class="dropdown-item has-icon">
                <i class="far fa-user"></i> ข้อมูลผู้ใช้งาน
              </a>
              <a href="features-activities.html" class="dropdown-item has-icon">
                <i class="fas fa-bolt"></i> Activities
              </a>
              <a href="features-settings.html" class="dropdown-item has-icon">
                <i class="fas fa-cog"></i> Settings
              </a>
              <div class="dropdown-divider"></div> -->
              <a href="<?=base_url()?>login/logout" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> ออกจากระบบ
              </a>
            </div>
          </li>
        </ul>
      </nav>
  <!-- </header> -->
</div>