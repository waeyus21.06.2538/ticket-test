<?php
	$_session_in = $this->session->userdata('logged_in');
	$_user_name_head 	= $_session_in['user_name'];
  $_user_img 			= base_url().'upload/users/'.$_session_in['user_img'];

?>
   <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="<?=base_url('admin/dasboard');?>">Ticket-System</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="<?=base_url('admin/dasboard');?>">TS</a>
          </div>
          <ul class="sidebar-menu">
              <li class="menu-header">เมนู</li>
              <?php
                if(isset($loadMenu) && count($loadMenu) > 0){
                  foreach($loadMenu as $item){
                    $_active = '';
                    if($data['menuHeader'] == $item['menu_path']){
                      $_active = 'active';
                    }
                    if($item['is_group'] < 1){
                    echo '<li class="'.$_active.'">';
                      echo '<a href="'.base_url().$item['menu_path'].$item['menu_url'].'" class="nav-link ">';
                        echo '<i class="'.$item['menu_icon'].'"></i> <span>'.$item['menu_name'].'</span>';
                      echo '</a>';
                    echo '</li>';
                    }

                    if($item['is_group'] > 0){
                      echo '<li class="nav-item dropdown '.$_active.'">';
                        echo '<a href="javascript:viod(0);" class="nav-link has-dropdown">';
                          echo '<i class="'.$item['menu_icon'].'"></i>';
                          echo '<span>'.$item['menu_name'].'</span>';
                        echo '</a>';
                        echo '<ul class="dropdown-menu">';
                          if(isset($item['map_menu_obj']) && count($item['map_menu_obj']) > 0){
                            foreach($item['map_menu_obj'] as $menu_list){
                              $_activeLine = '';
                              if($data['menuLineList'] == $menu_list['menu_code']){
                                $_activeLine = 'active';
                              }
                              echo '<li  class="'.$_activeLine.'">';
                                echo '<a href="'.base_url().$menu_list['menu_path'].$menu_list['menu_url'].'" class="nav-link">';
                                  echo $menu_list['menu_name'];
                                echo '</a>';
                              echo '</li>';
                            }
                          }
                        
                        echo '</ul>';
                      echo '</li>';
                    }
                  }
                }
                ?>
            </ul>
        </aside>
      </div>