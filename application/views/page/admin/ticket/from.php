<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?=$pagetitle?></h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="<?=base_url().'admin/ticket'?>">ตั๋ว</a></div>
        <div class="breadcrumb-item"><?=$pagetitle?></div>
      </div>
    </div>
    <div class="section-body">
      <div class="row mt-sm-4">
        <div class="col-12 col-md-12">
          <div class="card">
            <!-- <form  id="form-submit-contact" class="needs-validation"> autocomplete="off" -->
            <form id="form-submit-banner" class="needs-validation" enctype="multipart/form-data">
              <div class="card-header">
                <h4>แบบฟอร์ม</h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>ชื่อ</label>
                    <input type="text" class="form-control" name="ticket_name" value="<?=!empty($data['ticket']->ticket_name)? $data['ticket']->ticket_name : '';?>" placeholder="กรุณากรอกชื่อ" required>
                </div>

                <div class="form-group">
                  <label>รายละเอียด</label>
                  <textarea class="form-control" name="ticket_detail" placeholder="กรุณากรอกรายละเอียด" required><?=!empty($data['ticket']->ticket_detail)? $data['ticket']->ticket_detail : '';?></textarea>
                </div>

                <div class="form-group">
                  <label>ข้อมูลการติดต่อกลับ</label>
                  <textarea class="form-control" name="ticket_contact_information" placeholder="กรุณากรอกข้อมูลการติดต่อกลับ" required><?=!empty($data['ticket']->ticket_contact_information)? $data['ticket']->ticket_contact_information : '';?></textarea>
                </div>

                
              </div>
              <div class="card-footer text-right">
                <input type="hidden" name="mode" value="<?=!empty($mode)? $mode : '' ?>">
                <input type="hidden" name="ticket_no" value="<?=!empty($data['ticket']->ticket_no)? $data['ticket']->ticket_no : '' ?>">
                <button type="submit" class="btn btn-primary" id="btn-submit-item" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw'></i> กำลังบันทึกข้อมูล" >บันทึก</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<script>

  var post_url = "<?=$form_action?>";

  // this is the id of the form
  $("#form-submit-banner").submit(function(e) {
    e.preventDefault();
    setButtonLoading("#btn-submit-item");

    $.ajax({
      type: "POST",
      url: post_url,
      // data: $("#form-submit-banner").serialize(),
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData:false,
      success: function(data){
        if(data.status > 0){
          //icon ,message ,title ,color
          iziToast.success({
            title: data.title,
            message: data.message,
            position: 'topRight'
          });
          // setInputValue();
          setButtonReset("#btn-submit-item", 1000);
          setTimeout(function () {
            window.location.replace("<?=base_url()?>" + "admin/ticket");
          }, 5000);
        }else{
          iziToast.error({
            title: data.title,
            message: data.message,
            position: 'topRight'
          });
          setButtonReset("#btn-submit-item", 1000);
        }
      },
      error: function (data) {
        console.log('An error occurred.');
        iziToast.error({
            title: data.title,
            message: data.message,
            position: 'topRight'
        });
        setButtonReset("#btn-submit-item", 1000);
      },
    });
  });

</script>