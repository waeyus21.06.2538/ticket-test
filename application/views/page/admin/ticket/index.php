<style>
.switch {
    display: inline-block;
    height: 22px;
    position: relative;
    width: 42px;
}

.switch input {
	display:none;
}

.slider {
	background-color: #ccc;
	bottom: 0;
	cursor: pointer;
	left: 0;
	position: absolute;
	right: 0;
	top: 0;
	transition: .4s;
}

.slider:before {
    background-color: #fff;
    bottom: 2px;
    content: "";
    height: 18px;
    left: 2px;
    position: absolute;
    transition: .4s;
    width: 18px;
}

input:checked + .slider {
	background-color: #66bb6a;
}

input:checked + .slider:before {
	transform: translateX(20px);
}

.slider.round {
	border-radius: 34px;
}

.slider.round:before {
	border-radius: 50%;
}
</style>


<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?=$pagetitle?></h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><?=$pagetitle?></div>
      </div>
    </div>
    <div class="section-body">
      <div class="row mt-sm-4">
        <div class="col-12 col-md-12">
          <div class="card">
            <div class="card-header">
              <!-- <h4>รายละเอียดติดต่อเรา</h4> -->
              <div>
                <div class="form-group">
                  <label>สถานะ</label>
                  <select class="form-control" id="search-status">
                    <option value="">all</option>
                    <option value="pending">pending</option>
                    <option value="accepted">accepted</option>
                    <option value="resolved">resolved</option>
                    <option value="rejected">rejected</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>วันที่</label>
                    <div class="d-flex">
                      <input type="date" class="form-control" name="str" value="">
                      <small class="ml-2 mr-2"> ถึง </small>
                      <input type="date" class="form-control" name="end" value="">
                    </div>  
                </div>
                <button type="button" id="search_date" class="btn btn-info ml-2">ค้นหา</button>
              </div>
              <div class="custom-btn-action">
                <a href="<?=$data['url_create']?>" class="btn btn-success m-1"><i class="fas fa-plus"></i> เพิ่มตั๋ว</a>
              </div>
            </div>
            <div class="card-body">
              <div class="section-body">
              <div class="card shadow">
                <div class="table-responsive p-2">

                  <!-- Table -->
                  <table id='ticket_data' class="table align-items-center table-striped">
                    <thead class="thead-light">
                      <tr>
                        <th scope="col" width="50px">ลัมดับ</th>
                        <th scope="col" width="100px">ชื่อ</th>
                        <th scope="col" width="160px">รายละเอียด</th>
                        <th scope="col" width="160px">ข้อมูลการติดต่อกลับ</th>
                        <th scope="col" width="100px">สถานะ</th>
                        <th scope="col" width="60px">เวลาที่สร้าง</th>
                        <th scope="col" width="60px">เวลาที่แก้ไข</th>
                        <th scope="col" width="50px"></th>
                      </tr>
                    </thead>
                  </table>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


 <!-- Script -->
 <script type="text/javascript">
  $(document).ready(function(){

    load_data();

    $(document).on('click', '#search_date', function () {
      load_data(); 
    });

    function load_data() {

      var table = $('#ticket_data');
      table.DataTable().destroy();
      
      table.DataTable({
        processing: true,
        serverSide: true, 
        ajax: {
            url: '<?=base_url()?>admin/ticket/ticket_data',
            type: 'POST',
            data: {
              status : $('#search-status').val(),
              s_date : $('[name="str"]').val(),
              e_date : $('[name="end"]').val()
            },
        },
        order: [[5, "desc"]],
        pageLength: 10,
        'columns': [
            { data: 'number', orderable: false },
            { data: 'ticket_name', orderable: false },
            { data: 'ticket_detail', orderable: false},
            { data: 'ticket_contact_information', orderable: false},
            { data: 'btn_status', orderable: false},
            { data: 'system_add_date', orderable: true},
            { data: 'system_update_date', orderable: false},
            { data: 'btn_action', orderable: false},
        ]
      });

    }


    var post_url = '<?=base_url()?>api/admin'; 

    $(document).on('change', '.status', function () {
      var ticket_no     = $(this).attr('data-id');
      var ticket_status = this.value;
      
      swal({
        title: "ยืนยันการแก้ไขข้อมูล",
        text: "กรุณากดปุมยืนยัน",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "ยืนยัน",
        cancelButtonClass: "btn-secondary",
        cancelButtonText: "ยกเลิก",
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function(isConfirm) {
        if (isConfirm) {   
          $.ajax({
          type: "POST",
          url: post_url + '/ticket/status',
            data: {
              ticket_no : ticket_no,
              ticket_status : ticket_status,
            },
            success: function (data) {
              if(data.status > 0){
                  iziToast.success({title: data.title,message: data.message,position: 'topRight'
                });
              }else{
                iziToast.error({title: data.title,message: data.message,position: 'topRight'
                });
              }
              setTimeout(function(){
                load_data();
              }, 5000);  
            },
            error: function (data) {},
          });
        } 

      });
      
    });

    $(document).on('click', '.delete-item', function () {
      var ticket_no = $(this).attr('data-id');
      swal({
        title: "ต้องการลบข้อมูล หรือไหม?",
        text: "กรุณากดปุมยืนยัน",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "ยืนยัน",
        cancelButtonClass: "btn-secondary",
        cancelButtonText: "ยกเลิก",
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function(isConfirm) {
        if (isConfirm) {   
          $.ajax({
          type: "POST",
          url: post_url + '/ticket/delete',
            data: {
              ticket_no : ticket_no,
            },
            success: function (data) {
              if(data.status > 0){
                  iziToast.success({title: data.title,message: data.message,position: 'topRight'
                });
              }else{
                iziToast.error({title: data.title,message: data.message,position: 'topRight'
                });
              }
              setTimeout(function(){
                load_data();
              }, 5000);    
            },
            error: function (data) {},
          });
        }
      });
    });


  });
</script>