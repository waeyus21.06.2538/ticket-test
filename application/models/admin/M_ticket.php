<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*********************	Controller	*********************/
/* Project		: 										*/
/* File name	: M_ticket								*/
/* Version		: 1.0.0									*/
/* Create Date	: 01/10/2020							*/
/* Create by	: waeyusree								*/
/* Email		: -										*/
/* Description	: -										*/
/********************************************************/

class M_ticket extends CI_Model {
	
	private $table			= 'tbl_ticket';
	private $id 			= 'ticket_no';
	private $is_delete 		= 'is_delete';

	// Table ---tbl_contact---
	function countAll() {
		$this->db->from($this->table);
		return  $this->db->count_all_results();
	}

	public function insert($value) {
        $this->db->insert($this->table, $value);
        return $this->db->insert_id();
	}

	public function update($id,$value) {	
		$this->db->where($this->id, $id);
		$query = $this->db->update($this->table, $value);
		return $query;
	}
	
	public function get_ticket() {
		$this->db->where($this->is_delete, 0);
		$this->db->order_by($this->id,'desc');
        $this->db->select('*');
        $query = $this->db->get($this->table);
        return $query;
	}
	
	public function get_ticketById($id) {
		$this->db->where($this->id, $id);
        $this->db->select('*');
        $query = $this->db->get($this->table);
        return $query;
	}
   

	public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->where($this->is_delete, 0)
                        ->select('*')
                        ->from($this->table)
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->where($this->is_delete, 0)
                        ->select('*')
                        ->from($this->table)
                        ->get();
        return $query->num_rows();
    }

    function _condition($param) 
    {   
         
        if ( isset($param['s_date']) && $param['e_date'] != "" ) {
            $this->db->where("DATE_FORMAT(system_add_date,'%Y-%m-%d') BETWEEN '{$param['s_date']}' AND '{$param['e_date']}'");
        }    
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('ticket_name', $param['search']['value'])
                    ->or_like('ticket_detail', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            // if ($param['order'][0]['column'] == 0) $columnOrder = "ticket_name";
			// if ($param['order'][0]['column'] == 1) $columnOrder = "ticket_detail"; 
			if ($param['order'][0]['column'] == 5) $columnOrder = "system_add_date";          
            
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if (!empty($param['status']) ) 
            $this->db->where('ticket_status', $param['status']);
    }



}
/* End of file m_ticket.php */
/* Location: ./application/models/m_ticket.php */