<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*********************	Controller	*********************/
/* Project		: 										*/
/* File name	: m_user								*/
/* Version		: 1.0.0									*/
/* Create Date	: 19/02/2018							*/
/* Create by	: 										*/
/* Email		: -										*/
/* Description	: -										*/
/********************************************************/

class M_user extends CI_Model {
	
	private $table			= 'tbl_user';
	private $code 			= 'user_code';
	private $code2		= 'user_name';
	private $is_active 		= 'is_active';
	private $is_delete 		= 'is_delete';

    function __construct() {
        
    }
	
	function countAll() {
		$this->db->from($this->table);
		return  $this->db->count_all_results();
	}

	function countByUserActiveDelete($is_active, $is_delete) {
		$this->db->where($this->is_active, $is_active);
		$this->db->where($this->is_delete, $is_delete);
		$this->db->from($this->table);
		return  $this->db->count_all_results();
	}

	function countByUserCodeLogin($user) {
		$this->db->where($this->table.'.user_login_name', $user);
		$this->db->from($this->table);
		return  $this->db->count_all_results();
	}
	
	function countBySearchActiveDelete($code, $is_active, $is_delete) {
		if(!empty($code)){
			$this->db->like($this->code2, $code);
		}

		$this->db->where($this->is_active, $is_active);
		$this->db->where($this->is_delete, $is_delete);
		$this->db->from($this->table);
		return  $this->db->count_all_results();
	}

    function view($is_active, $is_delete) {
		$this->db->where($this->is_active, $is_active);
		$this->db->where($this->is_delete, $is_delete);
		$query = $this->db->get($this->table);
		
		$return_array = array();
		if ($query) {
			$return_array['status'] = true;
			$return_array['results'] = $query->result();

			if($query->num_rows() > 0) {
				$return_array['message'] = "";
			} else {
				$return_array['message'] = "No data found.";
			}
		} else {
			trigger_error($this->db->_error_message(), E_USER_ERROR);			
			$return_array['status'] = false;
			$return_array['results'] = null;
			$return_array['message'] = $this->db->_error_message();
		}

		return $return_array;
    }

	function viewDataTable($is_active, $is_delete, $start = 0 ,$length	= 10) {
		$this->db->where($this->is_active, $is_active);
		$this->db->where($this->is_delete, $is_delete);
        $this->db->limit($length ,$start);
		$query = $this->db->get($this->table);
		$return_array = array();
		if ($query) {
			$return_array['status'] = true;
			$return_array['results'] = $query->result();

			if($query->num_rows() > 0) {
				$return_array['message'] = "";
			} else {
				$return_array['message'] = "No data found.";
			}
		} else {
			trigger_error($this->db->_error_message(), E_USER_ERROR);			
			$return_array['status'] = false;
			$return_array['results'] = null;
			$return_array['message'] = $this->db->_error_message();
		}

		return $return_array;
	}
	
	function viewByKey($code) {
		
		$this->db->where($this->code, $code);
		//get query and processing
		$query = $this->db->get($this->table);

		$return_array = array();
		if ($query) {
			$results = $query->result();
			$return_array['status'] = true;

			if($query->num_rows() > 0) {
				$return_array['results'] = $results[0];
				$return_array['message'] = "";
			} else {
				$return_array['results'] = null;
				$return_array['message'] = "No data found.";
			}
		} else {
			trigger_error($this->db->_error_message(), E_USER_ERROR);			
			$return_array['status'] = false;
			$return_array['results'] = null;
			$return_array['message'] = $this->db->_error_message();
		}

		return $return_array;
    }
	
	function viewByUserAuthen($user, $pass, $is_active, $is_delete) {
		
		$this->db->where($this->table.'.user_login_name', $user);
		$this->db->where($this->table.'.user_login_pass', $pass);
		$this->db->where($this->table.'.'.$this->is_active, $is_active);
		$this->db->where($this->table.'.'.$this->is_delete, $is_delete);
		//get query and processing
		$query = $this->db->get($this->table);
		
		$return_array = array();
		if ($query) {
			$results = $query->result();
			$return_array['status'] = true;

			if($query->num_rows() > 0) {
				$return_array['results'] = $results[0];
				$return_array['message'] = "";
			} else {
				$return_array['results'] = null;
				$return_array['message'] = "No data found.";
			}
		} else {
			trigger_error($this->db->_error_message(), E_USER_ERROR);			
			$return_array['status'] = false;
			$return_array['results'] = null;
			$return_array['message'] = $this->db->_error_message();
		}

		return $return_array;
    }
	
	function viewByUserProfile($code) {
		
		$this->db->where($this->table.'.'.$this->code, $code);
		//get query and processing
		$this->db->join($this->table2, $this->table2.'.'.$this->code.' = '.$this->table.'.'.$this->code);
		// $this->db->group_by($this->table.'.'.$this->code); // Produces: GROUP BY title
		$query = $this->db->get($this->table);
		
		$return_array = array();
		if ($query) {
			$results = $query->result();
			$return_array['status'] = true;

			if($query->num_rows() > 0) {
				$return_array['results'] = $results[0];
				$return_array['message'] = "";
			} else {
				$return_array['results'] = null;
				$return_array['message'] = "No data found.";
			}
		} else {
			trigger_error($this->db->_error_message(), E_USER_ERROR);			
			$return_array['status'] = false;
			$return_array['results'] = null;
			$return_array['message'] = $this->db->_error_message();
		}

		return $return_array;
	}
	
	function viewSearchDataTable($code, $is_active, $is_delete, $start = 0 ,$length	= 10) {

		if(!empty($code)){
			$this->db->like($this->code2, $code);
		}

		$this->db->where($this->is_active, $is_active);
		$this->db->where($this->is_delete, $is_delete);
        $this->db->limit($length ,$start);
		$query = $this->db->get($this->table);
		$return_array = array();
		if ($query) {
			$return_array['status'] = true;
			$return_array['results'] = $query->result();

			if($query->num_rows() > 0) {
				$return_array['message'] = "";
			} else {
				$return_array['message'] = "No data found.";
			}
		} else {
			trigger_error($this->db->_error_message(), E_USER_ERROR);			
			$return_array['status'] = false;
			$return_array['results'] = null;
			$return_array['message'] = $this->db->_error_message();
		}

		return $return_array;
	}

}
  
/* End of file m_user.php */
/* Location: ./application/models/m_user.php */