<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*********************	Controller	*********************/
/* Project		: 										*/
/* File name	: m_menu_group							*/
/* Version		: 1.0.0									*/
/* Create Date	: 19/02/2018							*/
/* Create by	: 										*/
/* Email		: -										*/
/* Description	: -										*/
/********************************************************/

class M_menu_group extends CI_Model {
	
	private $table			= 'app_menu_group';
	private $table2			= 'map_menu';
	private $table3			= 'app_menu';
	private $code 			= 'menu_group_code';
	private $code2 			= 'menu_code';
	private $code3			= 'user_type_code';
	private $is_active 		= 'is_active';
	private $is_delete 		= 'is_delete';
	private $is_group 		= 'is_group';

    function __construct() {
        
    }
	
	function countAll($is_active) {
		$this->db->where($this->is_active, $is_active);
		$this->db->from($this->table);
		return  $this->db->count_all_results();
	}
	
	function countByActiveDelete($is_active, $is_delete) {
		$this->db->where($this->is_active, $is_active);
		$this->db->where($this->is_delete, $is_delete);
		$this->db->from($this->table);
		return  $this->db->count_all_results();
	}
    
    function countByDelete($is_delete) {
		$this->db->where($this->is_delete, $is_delete);
		$this->db->from($this->table);
		return  $this->db->count_all_results();
	}
	
    function view($code,$is_active, $is_delete) {
		$this->db->where($this->is_active, $is_active);
		$this->db->where($this->is_delete, $is_delete);
		$this->db->where($this->code3, $code);
		$this->db->order_by('position', 'ASC');
		$query = $this->db->get($this->table);
		
		$return_array = array();
		if ($query) {
			$return_array['status'] = true;
			$return_array['results'] = $query->result();

			if($query->num_rows() > 0) {
				$return_array['message'] = "";
			} else {
				$return_array['message'] = "No data found.";
			}
		} else {
			trigger_error($this->db->_error_message(), E_USER_ERROR);			
			$return_array['status'] = false;
			$return_array['results'] = null;
			$return_array['message'] = $this->db->_error_message();
		}

		return $return_array;
    }
    
    function viewDataTable($code, $is_delete, $start = 0 ,$length	= 10) {
		$this->db->where($this->code3, $code);
		$this->db->where($this->is_delete, $is_delete);
        $this->db->limit($length ,$start);
		$query = $this->db->get($this->table);
		$return_array = array();
		if ($query) {
			$return_array['status'] = true;
			$return_array['results'] = $query->result();

			if($query->num_rows() > 0) {
				$return_array['message'] = "";
			} else {
				$return_array['message'] = "No data found.";
			}
		} else {
			trigger_error($this->db->_error_message(), E_USER_ERROR);			
			$return_array['status'] = false;
			$return_array['results'] = null;
			$return_array['message'] = $this->db->_error_message();
		}

		return $return_array;
    }

	function viewByKey($code) {
		$this->db->where($this->code, $code);
		//get query and processing
		$query = $this->db->get($this->table);

		$return_array = array();
		if ($query) {
			$results = $query->result();
			$return_array['status'] = true;

			if($query->num_rows() > 0) {
				$return_array['results'] = $results[0];
				$return_array['message'] = "";
			} else {
				$return_array['results'] = null;
				$return_array['message'] = "No data found.";
			}
		} else {
			trigger_error($this->db->_error_message(), E_USER_ERROR);			
			$return_array['status'] = false;
			$return_array['results'] = null;
			$return_array['message'] = $this->db->_error_message();
		}

		return $return_array;
    }

	function viewMenuGroupJoin($code, $typeCode, $is_active, $is_delete) {
		$this->db->where($this->table2.'.'.$this->code3, $typeCode);
		$this->db->where($this->table2.'.'.$this->is_active, $is_active);
		$this->db->where($this->table2.'.'.$this->is_delete, $is_delete);
		$this->db->where($this->table2.'.'.$this->code, $code);
		$this->db->join($this->table3,$this->table3.'.'.$this->code2.'='.$this->table2.'.'.$this->code2);
		$query = $this->db->get($this->table2);
		
		$return_array = array();
		if ($query) {
			$return_array['status'] = true;
			$return_array['results'] = $query->result();

			if($query->num_rows() > 0) {
				$return_array['message'] = "";
			} else {
				$return_array['message'] = "No data found.";
			}
		} else {
			trigger_error($this->db->_error_message(), E_USER_ERROR);			
			$return_array['status'] = false;
			$return_array['results'] = null;
			$return_array['message'] = $this->db->_error_message();
		}

		return $return_array;
    }
}
  
/* End of file m_menu_group.php */
/* Location: ./application/models/m_menu_group.php */