<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LI_Controller extends CI_Controller {
	
	function __construct() {
		parent::__construct();

		$this->load->model(
			array(
				 'app/m_menu'
				,'app/m_menu_group'
				,'app/m_message'
				,'app/m_status'
			));

		$this->is_active		= 0; 
		$this->is_delete		= 0;
		$this->_data			= array();
		$this->user_type_code	= $this->session->userdata('logged_in')['user_type_code'];
	}

	public function setMenu(){
		// set menu group 0 
		$_menu_arr = array();
		$_obj_menu = $this->m_menu->view($this->user_type_code, $this->is_active,$this->is_delete, 0);
		if($_obj_menu['status'] == true && $_obj_menu['results'] != null){
			foreach($_obj_menu['results'] as $item){
				array_push($_menu_arr, array(
					 'menu_code' 	=> $item->menu_code
					,'menu_name' 	=> $item->menu_name
					,'menu_path' 	=> $item->menu_path
					,'menu_icon' 	=> $item->menu_icon
					,'is_group' 	=> $item->is_group
					,'menu_url'		=> $item->menu_url
					,'map_menu_obj' => ''
				));
			}
		}

		$_menu_group_arr = array();
		$_obj_menu_group = $this->m_menu_group->view($this->user_type_code, $this->is_active,$this->is_delete);
		if($_obj_menu_group['status'] == true && $_obj_menu_group['results'] != null){
			foreach($_obj_menu_group['results'] as $item){

				$_map_menu_arr = array();
				$_obj_map_menu = $this->m_menu_group->viewMenuGroupJoin($item->menu_group_code, $this->user_type_code, $this->is_active, $this->is_delete);
				if($_obj_map_menu['status'] == true && $_obj_map_menu['results'] != null){
                    foreach ($_obj_map_menu['results'] as $mapMenu) {
						array_push($_map_menu_arr, array(
							 'menu_code' 	=> $mapMenu->menu_code
							,'menu_name' 	=> $mapMenu->menu_name
							,'menu_path'	=> $mapMenu->menu_path
							,'menu_icon' 	=> $mapMenu->menu_icon
							,'menu_url'		=> $mapMenu->menu_url
						));
                    }
				}

				array_push($_menu_group_arr, array(
					 'menu_code' 	=> $item->menu_group_code
					,'menu_name' 	=> $item->menu_name
					,'menu_path'	=> $item->menu_path
					,'menu_icon' 	=> $item->menu_icon
					,'is_group'		=> 1
					,'map_menu_obj' => $_map_menu_arr
					,'menu_url'		=> $item->menu_url
				));
			}
		}

		return array_merge($_menu_arr,$_menu_group_arr);

	}

	public function setMessage(){
		$_message_arr = array();
		$_obj_message = $this->m_message->view($this->is_active,$this->is_delete);
		if($_obj_message['status'] == true && $_obj_message['results'] != null){
			foreach($_obj_message['results'] as $item){
				$_message_arr[$item->message_action] = array(
					 'message_code' 	=> $item->message_code
					,'message_action' 	=> $item->message_action
					,'message_name' 	=> $item->message_name
					,'message_detail' 	=> $item->message_detail
				);
			}
		}

		return $_message_arr;
	}

	public function getMessage($action){
		$_obj_message = $this->setMessage();
		return $_obj_message[$action];
	}

	public function setStatus(){
		$_status_arr = array();
		$_obj_status = $this->m_status->view($this->is_active, $this->is_delete);
		if($_obj_status['status'] == true && $_obj_status['results'] != null){
			foreach($_obj_status['results'] as $item){
				$_status_arr[$item->active_code] = array(
					 'active_code' 	=> $item->active_code
					,'active_name' 	=> $item->active_name
				);
			}
		}

		return $_status_arr;
	}
	
	public function getStatus($action){
		$_obj_status = $this->setStatus();
		return $_obj_status[$action];
	}

	public function setBase64_encode($code){
		return base64_encode($code);
	}

	public function setBase64_decode($code){
		return base64_decode($code);
	}
	
	public function setNumberFormat($data){
		return number_format($data,2);
	}

	public function getDate($data){
		list($yy,$mm,$dd) = explode("-",$data);
		$mmm = $mm-1;
		$yyy = $yy+543;
		$month_names = array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฏาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");		//$month_names = array("January","February","March","April","May","June","July","August","September","October","November","December");
		$dateday = intval($dd)." ".$month_names[intval($mmm)]." ".$yyy;
		return $dateday;
	}

	public function getDateTime($data){
		$time = substr($data,11);
		list($yy,$mm,$dd) = explode("-",$data);
		$mmm = $mm-1;
		$yyy = $yy+543;
		$month_names = array("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$dateday = intval($dd)." ".$month_names[intval($mmm)]." ".$yyy." ".$time;
		return $dateday;
	}

	public function loadPageView($_data){
		$_menu['loadMenu'] = $this->setMenu();
		$_mergedata = array_merge($_data,$_menu);
		$this->load->view('template/admin/body' ,$_mergedata);
	}

	public function loadPageFrontend($_data){
		// $_menu['loadMenu'] = $this->setMenu();
		// $_mergedata = array_merge($_data,$_menu);
		$this->load->view('template/frontend/body', $_data);
	}

	public function sendMailConfig(){

		$config = Array(
			'protocol' 		=> 'smtp'
			,'smtp_host' 	=> 'outgoing.mail.go.th'
			,'smtp_port' 	=> '465'
			,'smtp_timeout' => '7'
			,'smtp_user' 	=> 'ruslee.seebu@dga.or.th' // change it to yours
			,'smtp_pass' 	=> 'Ruslee0016' // change it to yours
			,'smtp_crypto'	=> 'ssl'
			,'mailtype' 	=> 'html'
			,'charset' 		=> 'utf-8'
			,'wordwrap' 	=> TRUE
		);
		
		return $config;
	}

	function arr($arr)
	{
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}

	//อัปโหลด 1 ภาพ
	public function setUploadOptions($path){
		$config['upload_path'] = getcwd().'/upload/'.$path.'/';
		$config['allowed_types'] = '*';
		$config['remove_spaces'] = true;
		$config['file_name']	 = date('YmdHis');

		// $config['image_library'] = 'gd2';
		// $config['quality'] = '50%';
		// $this->load->library('image_lib', $config);
		// $this->image_lib->resize();
		
		return $config;
	}

	//อัปโหลดหลายภาพ
	public function do_uploadMultiple($file, $type = TRUE, $path) {
		
		$upload_path='/upload/'.$path;
		if (!is_dir($upload_path )):
			@mkdir('./'.$upload_path , 0777, true);
		endif;

		//config file
		$config['upload_path']          = 'upload/'.$path;
		$config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|bmp|doc|docx|txt|xls|xlsx|csv|pptx|zip|7z';
		$config['encrypt_name']         = TRUE;

		//rename input file name array
		$fileReNamePost 				= explode('[]', $file);
		$data 							= array();

		// Looping all files
		  
		if(isset($_FILES[$fileReNamePost[0]]['name']) && count($_FILES[$fileReNamePost[0]]['name']) > 0):

			$i = 0;

			foreach($_FILES[$fileReNamePost[0]]['name'] as $filename):

				$_FILES['files[]']['name'] 		=  $filename;
				$_FILES['files[]']['type'] 		=  $_FILES[$fileReNamePost[0]]['type'][$i];
				$_FILES['files[]']['tmp_name'] 	=  $_FILES[$fileReNamePost[0]]['tmp_name'][$i];
				$_FILES['files[]']['error'] 		=  $_FILES[$fileReNamePost[0]]['error'][$i];
				$_FILES['files[]']['size'] 		=  $_FILES[$fileReNamePost[0]]['size'][$i];
				$config['file_name'] 			=  $filename;

				//Load upload library
				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('files[]')):
					$data['filenames'][] = array('error' => $this->upload->display_errors());
				else:
					$data['filenames'][] = array('index' => $this->upload->data());
	
					// $value = 'upload/'.$path.$this->upload->data('file_name')[$i];
					// $this->resize($value);
					

				endif;

			
			$i++;

			endforeach;	

		endif;

		return $data;
	}

	
	public function statistic(){
		$this->load->library('user_agent');
		$_statistic_arr = array();
	
		// browser
		$agent_browser = $this->agent->browser();
		$_statistic_arr['agent_browser'] = $agent_browser;
	
		// phone
		$agent_phone = $this->agent->mobile();
		$_statistic_arr['agent_phone'] = $agent_phone;
		
		// system
		$agent_system = $this->agent->platform(); // Platform info (Windows, Linux, Mac, etc.)
		$_statistic_arr['agent_system'] = $agent_system;

		return $_statistic_arr;
	}

}
