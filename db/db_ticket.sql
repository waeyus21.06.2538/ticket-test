-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2021 at 06:37 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ticket`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_menu`
--

CREATE TABLE `app_menu` (
  `menu_code` int(11) NOT NULL,
  `user_type_code` varchar(64) DEFAULT NULL,
  `menu_name` varchar(200) NOT NULL,
  `menu_path` varchar(200) NOT NULL,
  `menu_url` varchar(100) DEFAULT NULL,
  `menu_icon` varchar(100) NOT NULL,
  `position` int(11) NOT NULL DEFAULT 0,
  `is_group` int(1) NOT NULL DEFAULT 0,
  `is_active` int(1) NOT NULL DEFAULT 0,
  `is_delete` int(1) NOT NULL DEFAULT 0,
  `system_add_date` datetime NOT NULL,
  `system_update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `app_menu`
--

INSERT INTO `app_menu` (`menu_code`, `user_type_code`, `menu_name`, `menu_path`, `menu_url`, `menu_icon`, `position`, `is_group`, `is_active`, `is_delete`, `system_add_date`, `system_update_date`) VALUES
(1, '5A7F141E7E2A1', 'ข้อมูลผู้ใช้งาน', 'admin/usersad/profile', '', 'fas fa-user', 1, 1, 0, 0, '2019-06-28 10:00:31', '2019-06-28 10:00:31'),
(2, '5A7F141E7E2A1', 'Ticket', 'admin/ticket', '', 'fas fa-ticket-alt', 1, 0, 0, 0, '2019-06-28 10:00:31', '2019-06-28 10:00:31');

-- --------------------------------------------------------

--
-- Table structure for table `app_menu_group`
--

CREATE TABLE `app_menu_group` (
  `menu_group_code` int(11) NOT NULL,
  `user_type_code` varchar(64) DEFAULT NULL,
  `menu_name` varchar(200) NOT NULL,
  `menu_path` varchar(200) DEFAULT NULL,
  `menu_url` varchar(100) DEFAULT NULL,
  `menu_icon` varchar(100) NOT NULL,
  `position` int(11) NOT NULL DEFAULT 0,
  `is_active` int(1) NOT NULL DEFAULT 0,
  `is_delete` int(1) NOT NULL DEFAULT 0,
  `system_add_date` datetime NOT NULL,
  `system_update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `app_menu_group`
--

INSERT INTO `app_menu_group` (`menu_group_code`, `user_type_code`, `menu_name`, `menu_path`, `menu_url`, `menu_icon`, `position`, `is_active`, `is_delete`, `system_add_date`, `system_update_date`) VALUES
(1, '5A7F141E7E2A1', 'ตั้งค่า', 'setting', NULL, 'fas fa-cog', 5, 1, 0, '2018-08-29 10:38:00', '2018-08-29 10:38:00'),
(2, '5A7F141E7E2A1', 'จัดการข้อมูล', 'management', NULL, 'fa fa-edit', 1, 1, 0, '2018-10-29 11:23:46', '2018-10-29 11:23:46');

-- --------------------------------------------------------

--
-- Table structure for table `app_message`
--

CREATE TABLE `app_message` (
  `message_code` int(11) NOT NULL,
  `message_action` varchar(200) DEFAULT NULL,
  `message_name` varchar(200) NOT NULL,
  `message_detail` text NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT 0,
  `is_delete` int(1) NOT NULL DEFAULT 0,
  `system_add_date` datetime NOT NULL,
  `system_update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `app_message`
--

INSERT INTO `app_message` (`message_code`, `message_action`, `message_name`, `message_detail`, `is_active`, `is_delete`, `system_add_date`, `system_update_date`) VALUES
(1, 'ADD', 'เพิ่มข้อมูล', 'บันทึกข้อมูลเรียบร้อยแล้ว', 0, 0, '2018-08-29 10:38:00', '2018-08-29 10:38:00'),
(2, 'EDIT', 'แก้ไขข้อมูล', 'แก้ไขข้อมูลเรียบร้อยแล้ว', 0, 0, '2018-08-29 10:38:00', '2018-08-29 10:38:00'),
(3, 'DELETE', 'ลบข้อมูล', 'ลบข้อมูลเรียบร้อยแล้ว', 0, 0, '2018-08-29 10:38:00', '2018-08-29 10:38:00'),
(4, 'ERROR', 'เกิดข้อผิดพลาด', 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง', 0, 0, '2018-08-29 10:38:00', '2018-08-29 10:38:00'),
(5, 'VALIDATION', 'เกิดข้อผิดพลาด', 'กรุณากรอกข้อมูลให้ครบ กรุณาลองใหม่', 0, 0, '2018-08-29 10:38:00', '2018-08-29 10:38:00'),
(6, 'USERVALIDATION', 'เกิดข้อผิดพลาด', 'มีชื่อผู้ใช้งานนี้ในระบบแล้ว กรุณาลองใหม่อีกครั้ง', 0, 0, '2018-08-29 10:38:00', '2018-08-29 10:38:00'),
(7, 'PASSWORDCOMFIRM', 'เกิดข้อผิดพลาด', 'รหัสผ่านไม่ตรงกัน กรุณาลองใหม่อีกครั้ง', 0, 0, '2018-08-29 10:38:00', '2018-08-29 10:38:00'),
(8, 'USERVALIDATIONPASSWORD', 'เกิดข้อผิดพลาด', 'รหัสผ่านปัจจุบันไม่ตรงกัน กรุณาลองใหม่อีกครั้ง', 0, 0, '2018-08-29 10:38:00', '2018-08-29 10:38:00'),
(9, 'WARNING', 'เกิดข้อผิดพลาด', 'รหัสสินค้านี้มีอยู่แล้วในระบบ กรุณากรอกใหม่อีกครั้ง', 0, 0, '2020-10-08 10:37:48', '2020-10-08 10:37:56');

-- --------------------------------------------------------

--
-- Table structure for table `app_status`
--

CREATE TABLE `app_status` (
  `active_code` int(11) NOT NULL,
  `active_name` varchar(255) DEFAULT NULL,
  `is_active` int(1) DEFAULT 0,
  `is_delete` int(1) DEFAULT 0,
  `system_add_date` datetime DEFAULT NULL,
  `system_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `app_status`
--

INSERT INTO `app_status` (`active_code`, `active_name`, `is_active`, `is_delete`, `system_add_date`, `system_update_date`) VALUES
(1, 'ปิด', 0, 0, '2018-09-13 14:00:00', '2018-09-13 14:00:00'),
(2, 'เปิด', 0, 0, '2018-09-13 14:00:00', '2018-09-13 14:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `map_menu`
--

CREATE TABLE `map_menu` (
  `id` int(11) NOT NULL,
  `menu_group_code` varchar(64) NOT NULL,
  `menu_code` int(11) NOT NULL,
  `user_type_code` varchar(64) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT 0,
  `is_delete` int(1) NOT NULL DEFAULT 0,
  `system_add_date` datetime NOT NULL,
  `system_update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `map_menu`
--

INSERT INTO `map_menu` (`id`, `menu_group_code`, `menu_code`, `user_type_code`, `is_active`, `is_delete`, `system_add_date`, `system_update_date`) VALUES
(3, '1', 1, '5A7F141E7E2A1', 0, 0, '2019-06-28 10:09:33', '2019-06-28 10:09:33'),
(4, '1', 3, '5A7F141E7E2A1', 0, 0, '2019-06-28 10:09:30', '2019-06-28 10:09:33'),
(5, '1', 4, '5A7F141E7E2A1', 0, 0, '2019-06-28 10:10:12', '2019-06-28 10:10:15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ticket`
--

CREATE TABLE `tbl_ticket` (
  `ticket_no` int(11) NOT NULL,
  `ticket_name` varchar(100) DEFAULT '',
  `ticket_detail` text DEFAULT NULL,
  `ticket_contact_information` text DEFAULT NULL,
  `ticket_status` varchar(20) DEFAULT NULL,
  `is_delete` int(1) DEFAULT 0,
  `system_add_date` datetime DEFAULT NULL,
  `system_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_ticket`
--

INSERT INTO `tbl_ticket` (`ticket_no`, `ticket_name`, `ticket_detail`, `ticket_contact_information`, `ticket_status`, `is_delete`, `system_add_date`, `system_update_date`) VALUES
(1, 'เชียงราย... แม่สลอง-ดอยตุง', 'วันแรก : วัดห้วยปลากั้ง - วัดร่องเสือเต้น - สิงห์ปาร์ค - วัดร่องขุ่น - วัดพระแก้ว\r\nวันที่สอง : น้ำพุร้อนโป่งพระบาท - บ้านดำ - วัดท่าตอน - ดอยแม่สลอง', 'T. 02-898-1024 , 02-898-2470\r\nM. 094-9282665 ', 'pending', 0, '2021-01-11 11:52:31', '2021-01-11 12:17:21'),
(2, 'Lofoten: Island of Heaven & Arora', 'Foto Journey ยินดีพาท่านไปชม…..ดินแดนในเขตขั้วโลกเหนือ ที่มี แลนด์สเคป(Landscape) งดงามตระการตา จุดหมายปลายทางในฝันของ นักล่าแสงเหนือและนักถ่ายภาพทั่วโลก….', 'โทร. (662) 6619399 (อัตโนมัติ) แฟกซ์ (622) 6619788', 'pending', 0, '2021-01-11 12:06:19', '2021-01-11 12:06:19');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_code` varchar(64) NOT NULL,
  `user_login_name` varchar(255) DEFAULT NULL,
  `user_login_pass` varchar(50) DEFAULT NULL,
  `user_type_code` varchar(64) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_img` text DEFAULT NULL,
  `is_login` int(1) DEFAULT 0,
  `is_active` int(1) UNSIGNED DEFAULT 0,
  `is_delete` int(1) DEFAULT 0,
  `system_add_date` datetime DEFAULT NULL,
  `system_update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_code`, `user_login_name`, `user_login_pass`, `user_type_code`, `user_name`, `user_img`, `is_login`, `is_active`, `is_delete`, `system_add_date`, `system_update_date`) VALUES
('5A7F15114B751', 'user', '1234', '5A7F14B70BB44', 'User', '20180223071946.jpeg', 0, 0, 1, '2018-02-10 22:53:55', '2018-11-20 15:28:34'),
('5BA24C72E2C64', 'admin', '1234', '5A7F141E7E2A1', 'Admin', '20180223071946.jpeg', 0, 0, 0, '2018-02-10 22:53:55', '2018-11-20 15:28:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_type`
--

CREATE TABLE `tbl_user_type` (
  `user_type_code` varchar(64) NOT NULL,
  `user_type_name` varchar(255) DEFAULT NULL,
  `system_add_date` datetime DEFAULT NULL,
  `system_update_date` datetime DEFAULT NULL,
  `is_active` int(5) DEFAULT 0,
  `is_delete` int(5) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_user_type`
--

INSERT INTO `tbl_user_type` (`user_type_code`, `user_type_name`, `system_add_date`, `system_update_date`, `is_active`, `is_delete`) VALUES
('5A7F141E7E2A1', 'Admin', '2018-02-10 22:47:55', '2018-02-10 22:47:58', 0, 0),
('5A7F14B70BB44', 'User', '2018-02-10 22:50:34', '2018-02-10 22:50:37', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_menu`
--
ALTER TABLE `app_menu`
  ADD PRIMARY KEY (`menu_code`) USING BTREE;

--
-- Indexes for table `app_menu_group`
--
ALTER TABLE `app_menu_group`
  ADD PRIMARY KEY (`menu_group_code`) USING BTREE;

--
-- Indexes for table `app_message`
--
ALTER TABLE `app_message`
  ADD PRIMARY KEY (`message_code`) USING BTREE;

--
-- Indexes for table `app_status`
--
ALTER TABLE `app_status`
  ADD PRIMARY KEY (`active_code`) USING BTREE;

--
-- Indexes for table `map_menu`
--
ALTER TABLE `map_menu`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tbl_ticket`
--
ALTER TABLE `tbl_ticket`
  ADD PRIMARY KEY (`ticket_no`) USING BTREE;

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_code`) USING BTREE;

--
-- Indexes for table `tbl_user_type`
--
ALTER TABLE `tbl_user_type`
  ADD PRIMARY KEY (`user_type_code`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_menu`
--
ALTER TABLE `app_menu`
  MODIFY `menu_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `app_menu_group`
--
ALTER TABLE `app_menu_group`
  MODIFY `menu_group_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `app_message`
--
ALTER TABLE `app_message`
  MODIFY `message_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `app_status`
--
ALTER TABLE `app_status`
  MODIFY `active_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `map_menu`
--
ALTER TABLE `map_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_ticket`
--
ALTER TABLE `tbl_ticket`
  MODIFY `ticket_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
